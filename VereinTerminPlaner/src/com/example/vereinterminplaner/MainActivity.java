package com.example.vereinterminplaner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener {

	private Button login;
	String admin="Admin";
	public int ID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(com.example.vereinterminplaner.R.layout.activity_login);

		login=(Button)findViewById(R.id.Login);
		login.setOnClickListener(this);
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		// TODO Auto-generated method stub
		EditText pwdField = (EditText)findViewById(R.id.Passwort);
		String passwort = pwdField.getText().toString();
		if(passwort.length()==0)
			new AlertDialog.Builder(this).setMessage(R.string.error_pwd_missing).setNeutralButton(R.string.error_ok, null).show();
		
		EditText bnField = (EditText)findViewById(R.id.Benutzername);
		String benutzername = bnField.getText().toString();
		if(benutzername.length()==0)
			new AlertDialog.Builder(this).setMessage(R.string.error_bn_missing).setNeutralButton(R.string.error_ok, null).show();
		
		//Admin-Login
		
		new AlertDialog.Builder(this).setMessage(R.string.ok).setNeutralButton(R.string.error_ok, null).show();

		if(benutzername.equals(admin))
		{
			Intent test = new Intent(this, TerminAdmin.class);
			startActivity(test);
			this.finish();
		}
		else
		{
			Intent noAdmin= new Intent(this, TerminUser.class);
			startActivity(noAdmin);
			this.finish();
		}
		
		
	}
}


