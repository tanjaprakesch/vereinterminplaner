package com.example.vereinterminplaner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TerminUser extends Activity implements OnClickListener{

	private Button abmelden;
	private Button ja;
	private Button nein;
	
	public int zaehlerJa=0;
	public int zaehlerNein=0;
	MainActivity ma = new MainActivity();
	
	int ID = ma.ID;
	
	TerminAdmin ta;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.example.vereinterminplaner.R.layout.activity_terminuser);
		
		abmelden=(Button)findViewById(R.id.bAbmelden);
		abmelden.setOnClickListener(this);
		
		ja=(Button)findViewById(R.id.bJa);
		ja.setOnClickListener(this);
		
		nein=(Button)findViewById(R.id.bNein);
		nein.setOnClickListener(this);
	}
	
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.bAbmelden:
		{
			Intent abbrechenIntent= new Intent(this, MainActivity.class);
			startActivity(abbrechenIntent);
			this.finish();
		}
		break;
		
		case R.id.bJa:
		{
			zaehlerJa = zaehlerJa + 1;
			//ta=new TerminAdmin(zaehlerJa, zaehlerNein);

			//new AlertDialog.Builder(this).setMessage(zaehlerJa).setNeutralButton(R.string.ok, null).show();
			Intent JaIntent= new Intent(this, MainActivity.class);
			startActivity(JaIntent);
			this.finish();
		}
		break;
		
		
		case R.id.bNein:
		{
			zaehlerNein= zaehlerNein + 1;
			//ta=new TerminAdmin(zaehlerJa, zaehlerNein);

			Intent neinIntent= new Intent(this, MainActivity.class);
			startActivity(neinIntent);
			this.finish();
		}
		break;
		}
	}


}
