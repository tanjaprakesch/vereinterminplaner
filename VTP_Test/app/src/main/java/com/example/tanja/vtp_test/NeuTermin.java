package com.example.tanja.vtp_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.example.tanja.vtp_test.R;
import com.example.tanja.vtp_test.TerminAdmin;

public class NeuTermin extends Activity implements OnClickListener{

    private Button abbrechen;
    private Button speichern;
    public boolean gespeichert = false;
    public EditText typField;
    public String typ;
    public EditText tagField;
    public String tag;
    public EditText monatField;
    public String monat;
    public EditText jahrField;
    public String jahr;
    public EditText beginnField;
    public String beginn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.tanja.vtp_test.R.layout.activity_neutermin);

        abbrechen = (Button)findViewById(R.id.bAbbrechen);
        abbrechen.setOnClickListener(this);

        speichern = (Button)findViewById(R.id.bSpeichern);
        speichern.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        typField = (EditText)findViewById(R.id.eTTyp);
        tagField = (EditText)findViewById(R.id.eTTag);
        monatField = (EditText)findViewById(R.id.eTMonat);
        jahrField = (EditText)findViewById(R.id.eTJahr);
        beginnField = (EditText)findViewById(R.id.eTBeginn);

        typ = typField.getText().toString();
        tag = tagField.getText().toString();
        monat = monatField.getText().toString();
        jahr = jahrField.getText().toString();
        beginn = beginnField.getText().toString();

        switch(v.getId())
        {
            case R.id.bAbbrechen:
            {

                gespeichert = false;
                startActivity((new Intent(NeuTermin.this, TerminAdmin.class)));
                this.finish();

            }
            break;

            case R.id.bSpeichern:
            {
                if(typ.length()!=0 && tag.length()!=0 && monat.length()!=0 && jahr.length()!=0 && beginn.length()!=0)
                {
                    gespeichert = true;
                    Intent speichernIntent = new Intent(this, com.example.tanja.vtp_test.TerminAdmin.class);

                    startActivity(speichernIntent);
                    this.finish();

                }
                else
                    new AlertDialog.Builder(NeuTermin.this)
                            .setTitle("Error")
                            .setMessage("Please try again!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
            }
            break;

        }

    }

}
