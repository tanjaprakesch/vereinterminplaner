package com.example.tanja.vtp_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
/*import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
*/
//import android.app.ListActivity;
//import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.view.View.OnClickListener;
/*import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;
*/
public class TerminAdmin extends Activity implements OnClickListener{

    private Button abmelden;
    private Button neu;
    private ListView lv;
    int zaehlerJa, zaehlerNein;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.tanja.vtp_test.R.layout.activity_terminadmin);

        abmelden=(Button)findViewById(R.id.bAbmelden);
        abmelden.setOnClickListener(this);

        neu = (Button)findViewById(R.id.bNeuTermin);
        neu.setOnClickListener(this);

        FillListView();
    }

    private void FillListView()
    {
        lv = (ListView) findViewById(R.id.list);

        List<String> lvList = new ArrayList<String>();
        lvList.add("Samsung");
        lvList.add("Apple");
        lvList.add("Microsoft");
        lvList.add("Huawei");
        lvList.add("Sony");
        lvList.add("Samsung");
        lvList.add("Apple");
        lvList.add("Microsoft");
        lvList.add("Huawei");
        lvList.add("Sony");



        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                lvList );

        lv.setAdapter(arrayAdapter);
    }

    private String[] append(String[] zaehler, String string) {
        // TODO Auto-generated method stub
        return null;
    }

    public void onClick(View v){

        switch(v.getId())
        {
            case R.id.bAbmelden:
            {
                Intent abmeldenIntent= new Intent(this, MainActivity.class);
                startActivity(abmeldenIntent);
                this.finish();
            }
            break;

            case R.id.bNeuTermin:
            {
                Intent neuIntent = new Intent(this, NeuTermin.class);
                startActivity(neuIntent);
                this.finish();
            }
            break;

        }
    }

}


