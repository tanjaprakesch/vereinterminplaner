package com.example.tanja.vtp_test;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button login;
    String admin = "Admin";
    public int ID;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login=(Button)findViewById(R.id.Login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText pwdField = (EditText) findViewById(R.id.Passwort);
                String passwort = pwdField.getText().toString();

                EditText bnField = (EditText)findViewById(R.id.Benutzername);
                String benutzername = bnField.getText().toString();

                if(benutzername.length() == 0 && passwort.length()==0)
                {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("No Login")
                            .setMessage("Please try again!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

                else if(benutzername.length()==0)
                {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("No Username")
                            .setMessage("You haven't write an username. Please try again!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

                else if (passwort.length()==0)
                {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("No Password")
                            .setMessage("You haven't write a password. Please try again!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                }



               //Admin-Login

                else if(benutzername.equals(admin))
                {
                    startActivity(new Intent(MainActivity.this, TerminAdmin.class));
                    MainActivity.this.finish();
                }
                else
                {
                    startActivity(new Intent(MainActivity.this, TerminUser.class));
                    MainActivity.this.finish();
                }

                //startActivity((new Intent(MainActivity.this, TerminAdmin.class)));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
